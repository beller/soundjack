[Soundjack](https://soundjack.eu/) is developped by Prof. Dr.-Ing. Alexander Carôt, from 2006.

Soundjack is a member of the [fast research project (fast actuators, sensors and transceivers)](https://de.fast-zwanzig20.de/).

Playing live with someone abroad can be considered as a major challenge for musicians and sound engineers likewise. Due to cognitive, technical and purely musical problems and restrictions it had so far been impossible to reproduce a realistic rehearsing scenario as if in the same room. However, since nowadays the Internet offers sufficiant bandwidth and reliability it can satisfy the high demands and actually fulfill the extreme time critical restrictions. In order to achieve the most decent latency and quality of a network, the Soundjack software has been developed and applied in terms of a musical telepresence. Soundjack is a realtime communication system providing any quality- and latency- relevant parameter to the user. Depending on the physical distance, network capacities, the actual network conditions and the actual routing even musical interaction or at least compromised musical interaction is possible.

More info: https://soundjack.eu/
